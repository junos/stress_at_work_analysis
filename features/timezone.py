from collections.abc import Collection

import pandas as pd

from config.models import Participant, Timezone
from setup import db_engine, session


def get_timezone_data(usernames: Collection) -> pd.DataFrame:
    """
    Read the data from the proximity sensor table and return it in a dataframe.

    Parameters
    ----------
    usernames: Collection
        A list of usernames to put into the WHERE condition.

    Returns
    -------
    df_proximity: pd.DataFrame
        A dataframe of proximity data.
    """
    query_timezone = (
        session.query(Timezone, Participant.username)
        .filter(Participant.id == Timezone.participant_id)
        .filter(Participant.username.in_(usernames))
    )
    with db_engine.connect() as connection:
        df_timezone = pd.read_sql(query_timezone.statement, connection)
    return df_timezone
