COPE_ORIGINAL_MAX = 4
COPE_ORIGINAL_MIN = 1

DICT_COPE_QUESTION_IDS = {
    164: (
        "I took additional action to try to get rid of the problem",
        "Ik deed extra mijn best om er iets aan te doen",
        "Vložila sem dodaten napor, da bi rešila problem",
        "Vložil sem dodaten napor, da bi rešil problem",
    ),
    165: (
        "I concentrated my efforts on doing something about it",
        "Ik probeerde de situatie te verbeteren",
        "Svoje sile sem usmerila v reševanje nastale situacije",
        "Svoje sile sem usmeril v reševanje nastale situacije",
    ),
    166: (
        "I did what had to be done, one step at a time",
        "Ik deed stap voor stap wat nodig was",
        "Naredila sem, kar je bilo potrebno – korak za korakom",
        "Naredil sem, kar je bilo potrebno – korak za korakom",
    ),
    167: (
        "I took direct action to get around the problem",
        "Ik handelde vlug om het probleem te verhelpen",
        "Nekaj sem naredila, da sem zaobšla problem",
        "Nekaj sem naredil, da sem zaobšel problem",
    ),
    168: (
        "I tried to come up with a strategy about what to do",
        "Ik probeerde te verzinnen wat ik er aan kon doen",
        "Skušala sem najti ustrezen način za rešitev situacije",
        "Skušal sem najti ustrezen način za rešitev situacije",
    ),
    169: (
        "I made a plan of action",
        "Ik maakte een plan",
        "Naredila sem načrt za delovanje",
        "Naredil sem načrt za delovanje",
    ),
    170: (
        "I thought hard about what steps to take",
        "Ik dacht hard na over wat ik moest doen",
        "Dobro sem premislila, katere korake moram narediti, da rešim problem",
        "Dobro sem premislil, katere korake moram narediti, da rešim problem",
    ),
    171: (
        "I thought about how I might best handle the problem",
        "lk dacht na over hoe ik het probleem het best kon aanpakken",
        "Razmišljala sem, kaj bi bilo najbolje narediti s problemom",
        "Razmišljal sem, kaj bi bilo najbolje narediti s problemom",
    ),
    172: (
        "I asked people who have had similar experiences what they did",
        "Ik vroeg aan mensen met dergelijke ervaringen hoe zij reageerden",
        "Vprašala sem posameznike s podobnimi izkušnjami, kaj so storili",
        "Vprašal sem posameznike s podobnimi izkušnjami, kaj so storili",
    ),
    173: (
        "I tried to get advice from someone about what to do",
        "lk vroeg advies aan iemand",
        "Pri drugih sem poskušala dobiti nasvet, kaj naj storim",
        "Pri drugih sem poskušal dobiti nasvet, kaj naj storim",
    ),
    174: (
        "I talked to someone to find out more about the situation",
        "Ik sprak met iemand om meer te weten te komen over de situatie",
        "Z nekom sem se pogovorila, da bi izvedela še kaj o svojem problemu",
        "Z nekom sem se pogovoril, da bi izvedel še kaj o svojem problemu",
    ),
    175: (
        "I talked to someone who could do something concrete about the problem",
        "Ik sprak met iemand die iets aan het probleem kon doen",
        "Pogovorila sem se s kom, ki bi lahko naredil kaj konkretnega",
        "Pogovoril sem se s kom, ki bi lahko naredil kaj konkretnega",
    ),
    176: (
        "I talked to someone about how I felt",
        "Ik sprak met iemand over hoe ik mij voelde",
        "Z nekom sem se pogovorila o tem, kako sem se počutila",
        "Z nekom sem se pogovoril o tem, kako sem se počutil",
    ),
    177: (
        "I tried to get emotional support from friends or relatives",
        "Ik zocht steun bij vrienden of familie",
        "Skušala sem dobiti čustveno podporo prijateljev ali sorodnikov",
        "Skušal sem dobiti čustveno podporo prijateljev ali sorodnikov",
    ),
    178: (
        "I discussed my feelings with someone",
        "lk besprak mijn gevoelens met iemand",
        "O svojih občutkih sem se z nekom pogovorila",
        "O svojih občutkih sem se z nekom pogovoril",
    ),
    179: (
        "I got sympathy and understanding from someone",
        "Ik vroeg medeleven en begrip van iemand",
        "Poiskala sem naklonjenost in razumevanje drugih",
        "Poiskal sem naklonjenost in razumevanje drugih",
    ),
    180: (
        "I got upset and let my emotions out",
        "Ik raakte van streek",
        "Razburila sem se in to tudi pokazala",
        "Razburil sem se in to tudi pokazal",
    ),
    181: (
        "I let my feelings out",
        "Ik toonde mijn gevoelens",
        "Svojim čustvom sem dala prosto pot",
        "Svojim čustvom sem dal prosto pot",
    ),
    182: (
        "I felt a lot of emotional distress and I found myself expressing",
        "lk liet duidelijk blijken hoe ellendig ik mij voelde",
        "Doživljala sem veliko stresa in opažala, da sem čustva",
        "Doživljal sem veliko stresa in opažal, da sem čustva",
    ),
    183: (
        "I got upset, and I was really aware of it",
        "Ik merkte dat ik erg van streek was",
        "Razburila sem se in razmišljala samo o tem",
        "Razburil sem se in razmišljal samo o tem",
    ),
}
