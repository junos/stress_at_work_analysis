import unittest

from participants.query_db import get_usernames


class ParticipantsQuery(unittest.TestCase):
    def test_get_usernames(self):
        usernames_from_db = get_usernames()
        print(type(usernames_from_db))
        self.assertIsNotNone(usernames_from_db)
