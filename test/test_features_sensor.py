import unittest

import yaml
from pyprojroot import here

from machine_learning.features_sensor import *


class SensorFeaturesTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        with open(here("machine_learning/config/minimal_features.yaml"), "r") as file:
            cls.sensor_features_params = yaml.safe_load(file)

    def test_yaml(self):
        with open(here("machine_learning/config/minimal_features.yaml"), "r") as file:
            sensor_features_params = yaml.safe_load(file)
        self.assertIsInstance(sensor_features_params, dict)
        self.assertIsInstance(sensor_features_params.get("grouping_variable"), str)
        self.assertIsInstance(sensor_features_params.get("features"), dict)
        self.assertIsInstance(
            sensor_features_params.get("participants_usernames"), list
        )

    def test_participants_label(self):
        sensor_features = SensorFeatures(**self.sensor_features_params)
        self.assertRaises(ValueError, sensor_features.calculate_features)
