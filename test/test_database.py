import unittest

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from config.models import LightSensor, Participant
from setup import db_uri


class DatabaseConnection(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.engine = create_engine(db_uri, echo=True)
        Session = sessionmaker(bind=cls.engine)
        cls.session = Session()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.engine.dispose()
        cls.session.close()

    def test_connection(self):
        with self.engine.connect() as connection:
            self.assertIsNotNone(connection)
            connection.close()

    def test_get_participant(self):
        participant_0 = self.session.query(Participant).first()
        self.assertIsNotNone(participant_0)

    def test_get_light_data(self):
        light_0 = self.session.query(Participant).join(LightSensor).first()
        self.assertIsNotNone(light_0)
