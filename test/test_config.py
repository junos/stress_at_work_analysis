import os
import unittest

from dotenv import load_dotenv

load_dotenv()


class ConfigProject(unittest.TestCase):
    def test_env_variables(self):
        self.assertIsNotNone(os.getenv("DB_PASSWORD"))
