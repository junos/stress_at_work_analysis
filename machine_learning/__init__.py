QUESTIONNAIRE_IDS = {"PANAS": {"PA": 8.0, "NA": 9.0}}

QUESTIONNAIRE_IDS_RENAME = {}

for questionnaire in QUESTIONNAIRE_IDS.items():
    for k, v in questionnaire[1].items():
        QUESTIONNAIRE_IDS_RENAME[v] = k
