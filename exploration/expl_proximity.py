# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.11.4
#   kernelspec:
#     display_name: straw2analysis
#     language: python
#     name: straw2analysis
# ---

# %%
# %matplotlib inline
import datetime
import importlib
import os
import sys

import seaborn as sns
from pytz import timezone
from tabulate import tabulate

nb_dir = os.path.split(os.getcwd())[0]
if nb_dir not in sys.path:
    sys.path.append(nb_dir)

import participants.query_db

TZ_LJ = timezone("Europe/Ljubljana")

# %%
from features import helper, proximity

# %%
importlib.reload(proximity)

# %% [markdown]
# # Basic characteristics

# %%
df_proximity_nokia = proximity.get_proximity_data(["nokia_0000003"])
print(df_proximity_nokia)

# %%
df_proximity_nokia.double_proximity.value_counts()

# %% [markdown]
# `double_proximity` is "the distance to an object in front of the mobile device or binary presence (**manufacturer dependent**)."
#
# "Most proximity sensors return [the absolute distance, in cm](https://developer.android.com/guide/topics/sensors/sensors_position#sensors-pos-prox), but some return only near and far values.
#
# Note: Some proximity sensors return binary values that represent "near" or "far." In this case, the sensor usually reports its maximum range value in the far state and a lesser value in the near state. Typically, the far value is a value > 5 cm, but this can vary from sensor to sensor. You can determine a sensor's maximum range by using the getMaximumRange() method."

# %%
participants_inactive_usernames = participants.query_db.get_usernames()
df_proximity_inactive = proximity.get_proximity_data(participants_inactive_usernames)

# %%
df_proximity_inactive.double_proximity.describe()

# %%
sns.displot(
    data=df_proximity_inactive, x="double_proximity", binwidth=0.2, height=8,
)

# %%
df_proximity_inactive.double_proximity.value_counts()

# %% [markdown]
# We have already seen 5.0 and 5.000305 from the same device. What about other values?

# %% [markdown]
# # Participant proximity values

# %%
df_proximity_combinations = pd.crosstab(
    df_proximity_inactive.username, df_proximity_inactive.double_proximity
)
display(df_proximity_combinations)

# %% [markdown]
# Proximity labelled as 0 and 1.

# %%
df_proximity_combinations[df_proximity_combinations[1] != 0]

# %% [markdown]
# Proximity labelled as 0 and 8.

# %%
df_proximity_combinations[df_proximity_combinations[8] != 0]

# %% [markdown]
# The rest of the devices have proximity labelled as 0 and 5.00030517578125 or both of these values for "far".

# %%
df_proximity_combinations[
    (df_proximity_combinations[5.0] != 0)
    & (df_proximity_combinations[5.00030517578125] == 0)
]

# %%
df_proximity_combinations[
    (df_proximity_combinations[5.0] == 0)
    & (df_proximity_combinations[5.00030517578125] != 0)
]

# %%
df_proximity_combinations[
    (df_proximity_combinations[5.0] != 0)
    & (df_proximity_combinations[5.00030517578125] != 0)
]

# %% [markdown]
# # Features

# %%
df_proximity_inactive = helper.get_date_from_timestamp(df_proximity_inactive)

# %%
df_proximity_features = proximity.count_proximity(df_proximity_inactive, ["date_lj"])
display(df_proximity_features)
