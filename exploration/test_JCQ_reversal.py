# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.5
#   kernelspec:
#     display_name: straw2analysis
#     language: python
#     name: straw2analysis
# ---

# %%
import pandas as pd

from features.esm_JCQ import DICT_JCQ_DEMAND_CONTROL_REVERSE

# %%
limesurvey_questions = pd.read_csv(
    "E:/STRAWbaseline/survey637813+question_text.csv", header=None
).T

# %%
limesurvey_questions

# %%
limesurvey_questions[["code", "text"]] = limesurvey_questions[0].str.split(
    r"\.\s", expand=True, n=1
)

# %%
limesurvey_questions

# %%
demand_reverse_lime_rows = (
    limesurvey_questions["text"].str.startswith(" [Od mene se ne zahteva,")
    | limesurvey_questions["text"].str.startswith(" [Imam dovolj časa, da končam")
    | limesurvey_questions["text"].str.startswith(
        " [Pri svojem delu se ne srečujem s konfliktnimi"
    )
)
control_reverse_lime_rows = limesurvey_questions["text"].str.startswith(
    " [Moje delo vključuje veliko ponavljajočega"
) | limesurvey_questions["text"].str.startswith(
    " [Pri svojem delu imam zelo malo svobode"
)

# %%
demand_reverse_lime = limesurvey_questions[demand_reverse_lime_rows]
demand_reverse_lime.loc[:, "qid"] = demand_reverse_lime["code"].str.extract(
    r"\[(\d+)\]"
)
control_reverse_lime = limesurvey_questions[control_reverse_lime_rows]
control_reverse_lime.loc[:, "qid"] = control_reverse_lime["code"].str.extract(
    r"\[(\d+)\]"
)

# %%
limesurvey_questions.loc[89, "text"]

# %%
limesurvey_questions[limesurvey_questions["code"].str.startswith("JobEisen")]

# %%
demand_reverse_lime

# %%
control_reverse_lime

# %%
participant_info = pd.read_csv(
    "C:/Users/junos/Documents/FWO-ARRS/Analysis/straw2analysis/rapids/data/raw/p031/participant_baseline_raw.csv",
    parse_dates=["date_of_birth"],
)

# %%
participant_info_t = participant_info.T

# %%
rows_baseline = participant_info_t.index

# %%
rows_demand = rows_baseline.str.startswith("JobEisen") & ~rows_baseline.str.endswith(
    "Time"
)

# %%
rows_baseline[rows_demand]

# %%
limesurvey_control = (
    participant_info_t[rows_demand]
    .reset_index()
    .rename(columns={"index": "question", 0: "score_original"})
)

# %%
limesurvey_control

# %%
limesurvey_control["qid"] = (
    limesurvey_control["question"].str.extract(r"\[(\d+)\]").astype(int)
)

# %%
limesurvey_control["question"].str.extract(r"\[(\d+)\]").astype(int)

# %%
limesurvey_control["score"] = limesurvey_control["score_original"]

# %%
limesurvey_control["qid"][0]

# %%
rows_demand_reverse = limesurvey_control["qid"].isin(
    DICT_JCQ_DEMAND_CONTROL_REVERSE.keys()
)
limesurvey_control.loc[rows_demand_reverse, "score"] = (
    4 + 1 - limesurvey_control.loc[rows_demand_reverse, "score_original"]
)

# %%
JCQ_DEMAND = "JobEisen"
JCQ_CONTROL = "JobControle"
dict_JCQ_demand_control_reverse = {
    JCQ_DEMAND: {
        3: " [Od mene se ne zahteva,",
        4: " [Imam dovolj časa, da končam",
        5: " [Pri svojem delu se ne srečujem s konfliktnimi",
    },
    JCQ_CONTROL: {
        2: " |Moje delo vključuje veliko ponavljajočega",
        6: " [Pri svojem delu imam zelo malo svobode",
    },
}

# %%
limesurvey_control

# %%
test = pd.DataFrame(
    data={"question": "one", "score_original": 3, "score": 3, "qid": 10}, index=[0]
)

# %%
pd.concat([test, limesurvey_control]).reset_index()

# %%
limesurvey_control["score"].sum()

# %%
rows_demand_reverse

# %%
dict_JCQ_demand_control_reverse[JCQ_DEMAND].keys()

# %%
limesurvey_control

# %%
DEMAND_CONTROL_RATIO_MIN = 5 / (9 * 4)
DEMAND_CONTROL_RATIO_MAX = (4 * 5) / 9

JCQ_NORMS = {
    "F": {
        0: DEMAND_CONTROL_RATIO_MIN,
        1: 0.45,
        2: 0.52,
        3: 0.62,
        4: DEMAND_CONTROL_RATIO_MAX,
    },
    "M": {
        0: DEMAND_CONTROL_RATIO_MIN,
        1: 0.41,
        2: 0.48,
        3: 0.56,
        4: DEMAND_CONTROL_RATIO_MAX,
    },
}

# %%
JCQ_NORMS[participant_info.loc[0, "gender"]][0]

# %%
participant_info_t.index.str.startswith("JobControle")

# %%
columns_baseline = participant_info.columns

# %%
columns_demand = columns_baseline.str.startswith(
    "JobControle"
) & ~columns_baseline.str.endswith("Time")

# %%
columns_baseline[columns_demand]

# %%
participant_control = participant_info.loc[:, columns_demand]

# %%
participant_control["id"] = participant_control.index

# %%
participant_control

# %%
pd.wide_to_long(
    participant_control,
    stubnames="JobControle",
    i="id",
    j="qid",
    sep="[",
    suffix="(\\d+)]",
)
