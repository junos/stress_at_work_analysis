# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.11.2
#   kernelspec:
#     display_name: straw2analysis
#     language: python
#     name: straw2analysis
# ---

# %%
import os
import sys

from tabulate import tabulate

nb_dir = os.path.split(os.getcwd())[0]
if nb_dir not in sys.path:
    sys.path.append(nb_dir)

import participants.query_db

# %%
from features.screen import *

# %%
df_screen_nokia = get_screen_data(["nokia_0000003"])

# %%
print(df_screen_nokia)

# %%
participants_inactive_usernames = participants.query_db.get_usernames()
df_screen_inactive = get_screen_data(participants_inactive_usernames)

# %%
df_screen_inactive["screen_status"] = (
    df_screen_inactive["screen_status"]
    .astype("category")
    .cat.rename_categories(screen_status)
)
screen_freq = df_screen_inactive.value_counts("screen_status")
tabulate(screen_freq.to_frame(), tablefmt="html")

# %%
print(screen_status)

# %% [markdown]
# A typical sequence might be: off -> locked -> on -> unlocked (0 -> 2 -> 1 -> 3)

# %%
status_diff = df_screen_nokia.sort_values("timestamp")["screen_status"].diff()
status_diff.value_counts().to_frame()

# %% [markdown]
# But I have also seen off -> on -> unlocked (with 2 - locked missing) and off -> locked -> on -> off -> locked (*again*).
