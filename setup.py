import os

from dotenv import load_dotenv
from sqlalchemy import URL, create_engine
from sqlalchemy.orm import sessionmaker

load_dotenv()

testing: bool = False

db_password = os.getenv("DB_PASSWORD")

db_uri = URL.create(
    drivername="postgresql+psycopg2",
    username="staw_db",
    password=db_password,
    host="212.235.208.113",
    port=5432,
    database="staw",
)

if testing:
    db_engine = create_engine("sqlite:///:memory:", echo=True)
else:
    db_engine = create_engine(db_uri)

Session = sessionmaker(bind=db_engine)
session = Session()
